import { useState } from 'react'

import Header from './components/header'
import Home from './containers/home'
import Detail from './containers/detail'
import Footer from './components/footer'
import './App.css'
import {Routes, Route, Navigate} from 'react-router-dom'

function App() {
  

  return (<div className='App'>
    <Header />
    <main>
     <Routes>
       <Route exact path="/" element={<Home />}/>
       <Route exact path="/detail/:id" element={<Detail />}/>
       {/*Cette dernière route est faite pour éviter qu'on choisisse une autre route que celle du router, Navigate va rediriger vers l'url d'accueil */}
       <Route exact path="*" element={<Navigate to="/"/>}/>
     </Routes>
    </main>
    <Footer />
  </div>)
}

export default App