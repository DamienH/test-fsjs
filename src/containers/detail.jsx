import { Navigate, useParams } from 'react-router-dom'
import { getCompanyDetails, onLoadGetMovie } from '../api/moviedb'
import { useState, useEffect } from 'react'

const Detail = (props) => {
    const params = useParams()
    const [detailMovie, setDetailMovie] = useState(null)
    const [company, setCompany] = useState([])

    useEffect(() => {
        console.log(params.id)
        onLoadGetMovie(params.id)
            .then((res) => {
                console.log(res)
                setDetailMovie(res)
                if(company.length === 0){
                    let tab= []
                    res.production_companies.map((c)=>{
                        getCompanyDetails(c.id)
                        .then((response)=>{
                            tab.push(response)
                        })
                        .catch(err => console.log(err))
                    })
                }
                setCompany(tab)
            })
            .catch(err => console.log(err))
    }, [company])

    return (<section className="detail">
        {detailMovie !== null && <article>
                <h2>{detailMovie.original_title}</h2>
                <img src={`https://image.tmdb.org/t/p/w500${detailMovie.poster_path}`} />
                <p>Note: {detailMovie.vote_average}/10</p>
                <p style={{ width: 500, margin: "0 auto" }}>{detailMovie.overview}</p>

            </article>}
                
           
        
    </section>)
}

export default Detail

