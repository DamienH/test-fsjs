import { useState, useEffect } from 'react'
import axios from "axios"
import { SearchMoviesWithKeyWord } from '../api/moviedb'
import { Link } from 'react-router-dom'

const Home = (props) => {
    const [movies, setMovies] = useState([])
    const [keyword, setKeyword] = useState("")

    const search = (e) => {
        e.preventDefault()
        console.log(keyword)
        SearchMoviesWithKeyWord(keyword)
            .then((res) => {
                console.log(res)
                setMovies(res.results)
            })
            .catch(err => console.log(err))
    }

    return (<section>
        <h2>HomePage</h2>
        <form>
            <input type='text' onChange={(e) => {
                setKeyword(e.currentTarget.value)
            }} />
            <button onClick={search}>Rechercher</button>
        </form>
        <article className='info'>
            <div id='result'>
                {movies.map((movie) => {
                    return (<ul>
                        <li>
                            <Link key={movie.id} to={`/detail/${movie.id}`}>{movie.original_title}</Link>
                        </li>
                    </ul>)
                })}
            </div>
        </article>
    </section>

    )
}


export default Home;