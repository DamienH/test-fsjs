import {Link} from 'react-router-dom'
const Header = (props) => {
    return (<header className="App-header">
        <nav>
            <Link to="/">Accueil</Link>
        </nav>
    </header>)
}

export default Header