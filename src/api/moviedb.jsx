import axios from 'axios'
const API_KEY ="92a5c404045b7b68f4dec0bda33c2dbe"

export function SearchMoviesWithKeyWord(key){
    //requete ajax
    return axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${key}`)
    .then((response) => {
        return response.data
    })
    .catch(err=>console.log(err))
 
        //console.log(err)
}

export function onLoadGetMovie(id){
 //requete ajax
   return axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`)
    .then((response) =>{
        return response.data
    })
    .catch(err=>console.log(err))

}

export function getCompanyDetails(id){
 //requete ajax
   return axios.get(`https://api.themoviedb.org/3/movie/company_id=${id}`)
    .then((response)=>{
        return response.data
    })
    .cath(err=>console.log(err))
}
